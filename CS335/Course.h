//
//  Courses.h
//  CS335
//
//  Created by Adam Tay on 26/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Course : NSObject

@property (nonatomic) NSString *codeField;
@property (nonatomic) NSArray *semesterField;
@property (nonatomic) NSString *titleField;

@end
