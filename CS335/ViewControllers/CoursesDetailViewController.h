//
//  CoursesDetailViewController.h
//  CS335
//
//  Created by Adam Tay on 28/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoursesDetailViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *courseLabel;
@property (nonatomic, strong) NSString *courseName;

@end
