//
//  NewsViewController.h
//  CS335
//
//  Created by Adam Tay on 31/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate>

@property (nonatomic, strong) IBOutlet UITableView *newsTableView;
@property (nonatomic, strong) NSMutableArray *newsArray;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSMutableData *responseData;

@end
