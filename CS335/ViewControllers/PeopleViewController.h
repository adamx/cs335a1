//
//  PeopleViewController.h
//  CS335
//
//  Created by Adam Tay on 30/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *peopleTableView;
@property (nonatomic, weak) IBOutlet UIToolbar *toolbar;

@property (nonatomic, strong) NSArray *personsArray;

- (IBAction)popView:(UIBarButtonItem *)sender;

@end
