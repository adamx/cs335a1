//
//  PeopleListViewController.h
//  CS335
//
//  Created by Adam Tay on 30/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate>

@property (nonatomic, strong) IBOutlet UITableView *peopleTableView;
@property (nonatomic, strong) NSMutableArray *personsArray;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

- (NSData *)decodeBase64WithString:(NSString *)strBase64;

@end
