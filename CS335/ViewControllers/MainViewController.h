//
//  MainViewController.h
//  CS335
//
//  Created by Adam Tay on 1/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIImageView *homeImage;
@property (nonatomic, strong) IBOutlet UITableView *mainTableView;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
