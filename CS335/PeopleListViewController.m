//
//  PeopleListViewController.m
//  CS335
//
//  Created by Adam Tay on 30/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "PeopleListViewController.h"
#import "PersonDetailViewController.h"
#import "Person.h"

@implementation PeopleListViewController {
    NSXMLParser *parser;
    Person *personFeed;
    
    NSMutableString *upiField;
    NSString *element;
}

@synthesize peopleTableView, personsArray, activityIndicator, responseData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self.navigationItem setRightBarButtonItem:rightItem];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *peopleURL = [NSURL URLWithString:@"http://redsox.tcs.auckland.ac.nz/CSS/CSService.svc/people"];
    NSMutableURLRequest *peopleRequest = [NSURLRequest requestWithURL:peopleURL];
    [NSURLConnection connectionWithRequest:peopleRequest delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.peopleTableView indexPathForSelectedRow];
    if (selection) {
        [self.peopleTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    personsArray = [[NSMutableArray alloc] init];
    parser = [[NSXMLParser alloc] initWithData:responseData];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    element = elementName;
    
    if ([element isEqualToString:@"Person"]) {
        personFeed = [[Person alloc] init];
        upiField = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([element isEqualToString:@"uPIField"]) {
        [upiField appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"Person"]) {
        personFeed.upiField = upiField;

        [personsArray addObject:personFeed];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [peopleTableView reloadData];
    [self getVCard];
    
    [activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)getVCard{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (Person *person in personsArray) {
            NSString *URL = [NSString stringWithFormat:@"http://www.cs.auckland.ac.nz/our_staff/vcard.php?upi=%@", [person upiField]];
            NSData *personData = [NSData dataWithContentsOfURL:[NSURL URLWithString:URL]];
            NSString *vCardString = [[NSString alloc] initWithData:personData encoding:NSUTF8StringEncoding];
            
            //Parse vCard
            NSArray *vCard = [vCardString componentsSeparatedByString:@"\n"];
            
            NSString *name = [[[vCard objectAtIndex:4] substringFromIndex:2] stringByReplacingOccurrencesOfString:@";;" withString:@";"];
            NSArray *fullName = [name componentsSeparatedByString:@";"];
            NSString *position = [[vCard objectAtIndex:7] substringFromIndex:6];
            NSString *emailAddress = [[vCard objectAtIndex:8] substringFromIndex:15];
            NSString *phoneNumber = [[vCard objectAtIndex:3] substringFromIndex:44];
            NSData *image = nil;
            
            if ([[vCard objectAtIndex:11] length] > 100) {
                NSString *imageBase64 = [[vCard objectAtIndex:11] substringFromIndex:32];
                image = [self decodeBase64WithString:imageBase64];
            }
            
            person.vCardData = vCardString;
            person.title = fullName[2];
            person.position = position;
            person.fullName = [NSString stringWithFormat:@"%@ %@", fullName[1], fullName[0]];
            person.emailAddress = emailAddress;
            person.phoneNumber = [[phoneNumber stringByReplacingOccurrencesOfString:@"(External)" withString:@"x"] stringByReplacingOccurrencesOfString:@" (Internal)" withString:@""];
            person.imageData = image;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [peopleTableView reloadData];
            });
        }
    });
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [personsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"PersonCell";
    UITableViewCell *cell = [self.peopleTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    if ([[personsArray objectAtIndex:indexPath.row] fullName] == nil) {
        cell.textLabel.text = [[personsArray objectAtIndex:indexPath.row] upiField];
        cell.imageView.image = [UIImage imageNamed:@"defaultprofile.jpg"];
    
    } else {
        cell.textLabel.text = [[personsArray objectAtIndex:indexPath.row] fullName];
        
        if ([[personsArray objectAtIndex:indexPath.row] imageData] == nil) {
            cell.imageView.image = [UIImage imageNamed:@"defaultprofile.jpg"];
            
        } else {
            UIImage *originalImage = [UIImage imageWithData:[[personsArray objectAtIndex:indexPath.row] imageData]];
            UIImage *rotatedImage = [[UIImage alloc] initWithCGImage:originalImage.CGImage scale:1.0 orientation:UIImageOrientationUp];
            cell.imageView.image = rotatedImage;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showPersonDetail"]) {
        NSIndexPath *indexPath = [self.peopleTableView indexPathForSelectedRow];
        PersonDetailViewController *destViewController = segue.destinationViewController;

        destViewController.vCardData = [[personsArray objectAtIndex:indexPath.row] vCardData];
        destViewController.personTitle = [[personsArray objectAtIndex:indexPath.row] title];
        destViewController.position = [[personsArray objectAtIndex:indexPath.row] position];
        destViewController.fullName = [[personsArray objectAtIndex:indexPath.row] fullName];
        destViewController.emailAddress = [[personsArray objectAtIndex:indexPath.row] emailAddress];
        destViewController.phoneNumber = [[personsArray objectAtIndex:indexPath.row] phoneNumber];
        destViewController.imageData = [[personsArray objectAtIndex:indexPath.row] imageData];
    }
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSData *)decodeBase64WithString:(NSString *)strBase64 {
    static const short _base64DecodingTable[256] = {
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -1, -1, -2, -1, -1, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -1, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, 62, -2, -2, -2, 63,
        52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -2, -2, -2, -2, -2, -2,
        -2,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -2, -2, -2, -2, -2,
        -2, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
        41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2,
        -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2
    };
    
    const char *objPointer = [strBase64 cStringUsingEncoding:NSASCIIStringEncoding];
    size_t intLength = strlen(objPointer);
    int intCurrent;
    int i = 0, j = 0, k;
    
    unsigned char *objResult = calloc(intLength, sizeof(unsigned char));
    
    // Run through the whole string, converting as we go
    while ( ((intCurrent = *objPointer++) != '\0') && (intLength-- > 0) ) {
        if (intCurrent == '=') {
            if (*objPointer != '=' && ((i % 4) == 1)) {// || (intLength > 0)) {
                // the padding character is invalid at this point -- so this entire string is invalid
                free(objResult);
                return nil;
            }
            continue;
        }
        
        intCurrent = _base64DecodingTable[intCurrent];
        if (intCurrent == -1) {
            // we're at a whitespace -- simply skip over
            continue;
        } else if (intCurrent == -2) {
            // we're at an invalid character
            free(objResult);
            return nil;
        }
        
        switch (i % 4) {
            case 0:
                objResult[j] = intCurrent << 2;
                break;
                
            case 1:
                objResult[j++] |= intCurrent >> 4;
                objResult[j] = (intCurrent & 0x0f) << 4;
                break;
                
            case 2:
                objResult[j++] |= intCurrent >>2;
                objResult[j] = (intCurrent & 0x03) << 6;
                break;
                
            case 3:
                objResult[j++] |= intCurrent;
                break;
        }
        i++;
    }
    
    // mop things up if we ended on a boundary
    k = j;
    if (intCurrent == '=') {
        switch (i % 4) {
            case 1:
                // Invalid state
                free(objResult);
                return nil;
                
            case 2:
                k++;
                // flow through
            case 3:
                objResult[k] = 0;
        }
    }
    
    // Cleanup and setup the return NSData
    NSData * objData = [[NSData alloc] initWithBytes:objResult length:j];
    return objData;
}

@end