//
//  CourseListViewController.m
//  CS335
//
//  Created by Adam Tay on 26/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "CourseListViewController.h"
#import "Course.h"

@implementation CourseListViewController {
    NSMutableArray *coursesArray, *semesterOneArray, *semesterTwoArray, *summerSchoolArray;

    NSXMLParser *parser;
    Course *courseFeed;
    
    NSMutableString *codeField;
    NSMutableString *semesterField;
    NSMutableString *titleField;
    
    NSString *element;
}

@synthesize courseListTableView, activityIndicator, responseData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self.navigationItem setRightBarButtonItem:rightItem];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    semesterOneArray = [[NSMutableArray alloc] init];
    semesterTwoArray = [[NSMutableArray alloc] init];
    summerSchoolArray = [[NSMutableArray alloc] init];
    
    NSURL *courseURL = [NSURL URLWithString:@"http://redsox.tcs.auckland.ac.nz/CSS/CSService.svc/courses"];
    NSMutableURLRequest *courseRequest = [NSURLRequest requestWithURL:courseURL];
    [NSURLConnection connectionWithRequest:courseRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {        
    coursesArray = [[NSMutableArray alloc] init];
    parser = [[NSXMLParser alloc] initWithData:responseData];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    element = elementName;
    
    if ([element isEqualToString:@"Course"]) {
        courseFeed = [[Course alloc] init];
        codeField = [[NSMutableString alloc] init];
        semesterField = [[NSMutableString alloc] init];
        titleField = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([element isEqualToString:@"codeField"]) {
        [codeField appendString:string];
    
    } else if ([element isEqualToString:@"semesterField"]) {
        [semesterField appendString:string];
        
    } else if ([element isEqualToString:@"titleField"]) {
        [titleField appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"Course"]) {
        courseFeed.codeField = codeField;
        NSArray *semesters = [semesterField componentsSeparatedByString:@"; "];
        courseFeed.semesterField = semesters;
        courseFeed.titleField = titleField;

        [coursesArray addObject:courseFeed];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    for (Course *course in coursesArray) {
        for (NSString *semester in [course semesterField]) {
            if ([semester isEqualToString:@"Semester 1"]) {
                [semesterOneArray addObject:course];
            }
            
            if ([semester isEqualToString:@"Semester 2"]) {
                [semesterTwoArray addObject:course];
            }
            
            if ([semester isEqualToString:@"Summer School"]) {
                [summerSchoolArray addObject:course];
            }
        }
    }
    
    coursesArray = nil;
    coursesArray = [[NSMutableArray alloc] initWithObjects:summerSchoolArray, semesterOneArray, semesterTwoArray, nil];
    
    [courseListTableView reloadData];
    [activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionContents = [coursesArray objectAtIndex:section];
    NSUInteger rows = [sectionContents count];
    
    return rows;
}

- (NSUInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [coursesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *sectionContents = [coursesArray objectAtIndex:[indexPath section]];
    NSString *contentsForThisRow = [[sectionContents objectAtIndex:[indexPath row]] codeField];
    NSString *subtitleContent = [[sectionContents objectAtIndex:[indexPath row]] titleField];
    
    static NSString *simpleTableIdentifier = @"CourseCell";
    UITableViewCell *cell = [self.courseListTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = contentsForThisRow;
    cell.detailTextLabel.text = subtitleContent;
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"Summer School";
    } else if (section == 1) {
        return @"Semester 1";
    } else return @"Semester 2";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.courseListTableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
