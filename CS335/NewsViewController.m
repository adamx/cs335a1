//
//  NewsViewController.m
//  CS335
//
//  Created by Adam Tay on 31/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "NewsViewController.h"
#import "News.h"

@implementation NewsViewController {
    NSXMLParser *parser;
    News *newsFeed;
    
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *description;
    NSMutableString *pubDate;
    
    NSString *element;
}
@synthesize newsTableView, newsArray, activityIndicator, responseData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self.navigationItem setRightBarButtonItem:rightItem];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *newsURL = [NSURL URLWithString:@"http://www.cs.auckland.ac.nz/uoa/home/template/news_feed.rss?category=science_cs_news"];
    NSMutableURLRequest *newsRequest = [NSURLRequest requestWithURL:newsURL];
    [NSURLConnection connectionWithRequest:newsRequest delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.newsTableView indexPathForSelectedRow];
    if (selection) {
        [self.newsTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    newsArray = [[NSMutableArray alloc] init];
    parser = [[NSXMLParser alloc] initWithData:responseData];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        newsFeed = [[News alloc] init];
        
        title = [[NSMutableString alloc] init];
        link = [[NSMutableString alloc] init];
        description = [[NSMutableString alloc] init];
        pubDate = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
        
    } else if ([element isEqualToString:@"description"]) {
        [description appendString:string];
        
    } else if ([element isEqualToString:@"pubDate"]) {
        [pubDate appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
        newsFeed.title = title;
        newsFeed.link = link;
        newsFeed.description = description;
        newsFeed.pubDate = pubDate;
        
        [newsArray addObject:newsFeed];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [newsTableView reloadData];
    
    [activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"NewsCell";
    UITableViewCell *cell = [self.newsTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [[newsArray objectAtIndex:indexPath.row] title];
    cell.detailTextLabel.text = [[newsArray objectAtIndex:indexPath.row] pubDate];
    cell.textLabel.numberOfLines = 0;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.newsTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *url = [[newsArray objectAtIndex:indexPath.row] link];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
