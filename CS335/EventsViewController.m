//
//  EventsViewController.m
//  CS335
//
//  Created by Adam Tay on 1/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "EventsViewController.h"
#import "News.h"

@implementation EventsViewController {
    NSXMLParser *parser;
    News *eventsFeed;
    
    NSMutableString *title;
    NSMutableString *link;
    NSMutableString *description;
    NSMutableString *pubDate;
    
    NSString *element;
}
@synthesize eventsTableView, eventsArray, activityIndicator, responseData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self.navigationItem setRightBarButtonItem:rightItem];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *eventsURL = [NSURL URLWithString:@"http://www.cs.auckland.ac.nz/uoa/home/template/events_feed.rss?category=other_events"];
    NSMutableURLRequest *eventsRequest = [NSURLRequest requestWithURL:eventsURL];
    [NSURLConnection connectionWithRequest:eventsRequest delegate:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.eventsTableView indexPathForSelectedRow];
    if (selection) {
        [self.eventsTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    eventsArray = [[NSMutableArray alloc] init];
    parser = [[NSXMLParser alloc] initWithData:responseData];
    
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        eventsFeed = [[News alloc] init];
        
        title = [[NSMutableString alloc] init];
        link = [[NSMutableString alloc] init];
        description = [[NSMutableString alloc] init];
        pubDate = [[NSMutableString alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
        
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
        
    } else if ([element isEqualToString:@"description"]) {
        [description appendString:string];
        
    } else if ([element isEqualToString:@"pubDate"]) {
        [pubDate appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if ([elementName isEqualToString:@"item"]) {
        eventsFeed.title = title;
        eventsFeed.link = link;
        eventsFeed.description = description;
        eventsFeed.pubDate = pubDate;
        
        [eventsArray addObject:eventsFeed];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    [eventsTableView reloadData];
    
    [activityIndicator stopAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [eventsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"EventsCell";
    UITableViewCell *cell = [self.eventsTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [[eventsArray objectAtIndex:indexPath.row] title];
    cell.detailTextLabel.text = [[eventsArray objectAtIndex:indexPath.row] pubDate];
    cell.textLabel.numberOfLines = 0;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.eventsTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *url = [[eventsArray objectAtIndex:indexPath.row] link];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
