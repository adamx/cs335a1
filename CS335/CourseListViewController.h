//
//  CourseListViewController.h
//  CS335
//
//  Created by Adam Tay on 26/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate>

@property (nonatomic, strong) IBOutlet UITableView *courseListTableView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSMutableData *responseData;

@end
