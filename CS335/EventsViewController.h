//
//  EventsViewController.h
//  CS335
//
//  Created by Adam Tay on 1/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSXMLParserDelegate>

@property (nonatomic, strong) IBOutlet UITableView *eventsTableView;
@property (nonatomic, strong) NSMutableArray *eventsArray;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSMutableData *responseData;

@end
