//
//  PersonDetailViewController.h
//  CS335
//
//  Created by Adam Tay on 30/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonDetailViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSString *vCardData;
@property (nonatomic, strong) NSString *personTitle;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, strong) NSString *fullName;
@property (nonatomic, strong) NSString *emailAddress;
@property (nonatomic, strong) NSString *phoneNumber;
@property (nonatomic, strong) NSData *imageData;
@property (nonatomic, weak) IBOutlet UIImageView *profileImage;

- (IBAction)addContact:(UIButton *)sender;

@end
