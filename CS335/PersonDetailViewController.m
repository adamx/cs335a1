//
//  PersonDetailViewController.m
//  CS335
//
//  Created by Adam Tay on 30/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "PersonDetailViewController.h"
#import "MessageUI/MessageUI.h"
#import "AddressBook/AddressBook.h"
#import "AddressBookUI/AddressBookUI.h"

@implementation PersonDetailViewController {
    NSArray *personNameArray;
    NSArray *personEmailArray;
    NSArray *personContactArray;
    NSMutableArray *personsArray;
}

@synthesize tableView, vCardData, personTitle, position, fullName, emailAddress, phoneNumber, imageData, profileImage;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    fullName = [NSString stringWithFormat:@"%@ %@", personTitle, fullName];
    
    personNameArray = [NSArray arrayWithObjects:fullName, position, nil];
    personEmailArray = [NSArray arrayWithObjects:emailAddress, nil];
    personContactArray = [NSArray arrayWithObjects:phoneNumber, nil];
    
    personsArray = [[NSMutableArray alloc] initWithObjects:personNameArray, personEmailArray, personContactArray, nil];
    
    if (imageData != nil) {
        UIImage *originalImage = [UIImage imageWithData:imageData];
        UIImage *rotatedImage = [[UIImage alloc] initWithCGImage:originalImage.CGImage scale:1.0 orientation:UIImageOrientationUp];
        [profileImage setImage:rotatedImage];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.tableView indexPathForSelectedRow];
    if (selection) {
        [self.tableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionContents = [personsArray objectAtIndex:section];
    NSUInteger rows = [sectionContents count];
    
    return rows;
}

- (NSUInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [personsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *sectionOne = [[NSArray alloc] initWithObjects:@"Name:", @"Position:", nil];
    NSArray *sectionTwo = [[NSArray alloc] initWithObjects:@"Email:", nil];
    NSArray *sectionThree = [[NSArray alloc] initWithObjects:@"Phone:", nil];
    NSArray *content = [[NSArray alloc] initWithObjects:sectionOne, sectionTwo, sectionThree, nil];
    
    NSArray *detailContents = [content objectAtIndex:[indexPath section]];
    NSArray *sectionContents = [personsArray objectAtIndex:[indexPath section]];
    
    NSString *detailsForThisRow = [detailContents objectAtIndex:[indexPath row]];
    NSString *contentsForThisRow = [sectionContents objectAtIndex:[indexPath row]];
    
    static NSString *simpleTableIdentifier = @"PersonCell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = detailsForThisRow;
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    cell.detailTextLabel.text = contentsForThisRow;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1) {
        NSString *emailRecipient = [NSString stringWithFormat:@"mailto:%@", emailAddress];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:emailRecipient]];
    
    } else if (indexPath.section == 2) {
        NSArray *splitNumber = [phoneNumber componentsSeparatedByString:@" x "];
        NSString *phNum = [NSString stringWithFormat:@"tel:+649%@,%@", [[splitNumber[0] substringFromIndex:8] stringByReplacingOccurrencesOfString:@"-" withString:@""], splitNumber[1]];

        NSURL *phoneURL = [[NSURL alloc] initWithString:phNum];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addContact:(UIButton *)sender {
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (ABAddressBookGetAuthorizationStatus() == 3) {
            CFDataRef vCardRef = (__bridge CFDataRef)[vCardData dataUsingEncoding:NSUTF8StringEncoding];
            ABRecordRef defaultSource = ABAddressBookCopyDefaultSource(addressBook);
            CFArrayRef vCardPerson = ABPersonCreatePeopleInSourceWithVCardRepresentation(defaultSource, vCardRef);
           
            for (CFIndex i = 0; i < CFArrayGetCount(vCardPerson); i++) {
                ABRecordRef person = CFArrayGetValueAtIndex(vCardPerson, i);
                ABAddressBookAddRecord(addressBook, person, NULL);
            }
            
            ABAddressBookSave(addressBook, NULL);
        }
    });
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Contact Added!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

@end
