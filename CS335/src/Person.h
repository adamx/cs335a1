//
//  Person.h
//  CS335
//
//  Created by Adam Tay on 27/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject {
    NSString *upiField;
    
    NSString *title;
    NSString *position;
    NSString *fullName;
    NSString *emailAddress;
    NSString *phoneNumber;
    NSData *imageData;
}

@property (nonatomic, strong) NSString *upiField;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *position;
@property (nonatomic, retain) NSString *fullName;
@property (nonatomic, retain) NSString *emailAddress;
@property (nonatomic, retain) NSString *phoneNumber;
@property (nonatomic, retain) NSData *imageData;

@end
