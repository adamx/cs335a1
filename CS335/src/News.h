//
//  News.h
//  CS335
//
//  Created by Adam Tay on 31/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject {
    NSString *title;
    NSString *link;
    NSString *description;
    NSString *pubDate;
}

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *link;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *pubDate;

@end
