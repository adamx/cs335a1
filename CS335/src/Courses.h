//
//  Courses.h
//  CS335
//
//  Created by Adam Tay on 26/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Courses : NSObject {
    NSString *codeField;
    NSString *semesterField;
    NSString *titleField;
}

@property (nonatomic, retain) NSString *codeField;
@property (nonatomic, retain) NSString *semesterField;
@property (nonatomic, retain) NSString *titleField;

@end
