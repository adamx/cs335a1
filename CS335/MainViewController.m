//
//  MainViewController.m
//  CS335
//
//  Created by Adam Tay on 1/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "MainViewController.h"

@implementation MainViewController {
    NSArray *mainArray;
}

@synthesize homeImage, mainTableView, activityIndicator, responseData, refreshButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getURLImage];
    [refreshButton setEnabled:NO];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    mainArray = [[NSArray alloc] initWithObjects:@"Courses", @"People", @"Seminars", @"Events", @"News", @"Quick Links", @"About", nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.mainTableView indexPathForSelectedRow];
    if (selection) {
        [self.mainTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (void)getURLImage {
    NSURL *imageURL = [NSURL URLWithString:@"http://redsox.tcs.auckland.ac.nz/CSS/CSService.svc/home_image"];
    NSMutableURLRequest *imageRequest = [NSURLRequest requestWithURL:imageURL];
    [NSURLConnection connectionWithRequest:imageRequest delegate:self];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    UIImage *image = [UIImage imageWithData:responseData];
    
    [homeImage setImage:image];
    [refreshButton setEnabled:YES];
    [activityIndicator stopAnimating];
    [activityIndicator setHidden:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mainArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"MainCell";
    UITableViewCell *cell = [self.mainTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    cell.textLabel.text = [mainArray objectAtIndex:indexPath.row];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 120)];
    view.backgroundColor = [UIColor whiteColor];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSIndexPath *selection = [self.mainTableView indexPathForSelectedRow];
    
    switch (selection.row) {
        case 0:
            [self performSegueWithIdentifier:@"showCourses" sender:self];
            break;
            
        case 1:
            [self performSegueWithIdentifier:@"showPeople" sender:self];
            break;
            
        case 2:
            [self performSegueWithIdentifier:@"showSeminars" sender:self];
            break;
            
        case 3:
            [self performSegueWithIdentifier:@"showEvents" sender:self];
            break;
            
        case 4:
            [self performSegueWithIdentifier:@"showNews" sender:self];
            break;
            
        case 5:
            [self performSegueWithIdentifier:@"showQuickLinks" sender:self];
            break;
            
        case 6:
            [self performSegueWithIdentifier:@"showAbout" sender:self];
            break;
            
        default:
            break;
    }
}

- (IBAction)refreshImage:(UIButton *)sender {
    [refreshButton setEnabled:NO];
    [homeImage setImage:nil];
    [self getURLImage];
    
    [activityIndicator setHidden:NO];
    [activityIndicator startAnimating];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
@end
