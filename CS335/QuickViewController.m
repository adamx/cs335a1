//
//  QuickViewController.m
//  CS335
//
//  Created by Adam Tay on 6/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import "QuickViewController.h"

@implementation QuickViewController
@synthesize linksTableView, linksArray, urlArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [[self view] addGestureRecognizer:recognizer];
    
    linksArray = [NSArray arrayWithObjects:@"CS Website", @"Cecil", @"EC Mail", @"Student Services Online", @"Campus Map", nil];
    urlArray = [NSArray arrayWithObjects:@"http://cs.auckland.ac.nz", @"http://cecil.auckland.ac.nz", @"http://webmail.ec.auckland.ac.nz", @"http://student.auckland.ac.nz", @"http://web.env.auckland.ac.nz/public/maps/city.pdf", nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSIndexPath *selection = [self.linksTableView indexPathForSelectedRow];
    if (selection) {
        [self.linksTableView deselectRowAtIndexPath:selection animated:YES];
    }
}

- (NSUInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [linksArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"LinksCell";
    UITableViewCell *cell = [self.linksTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [linksArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.linksTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *url = [urlArray objectAtIndex:indexPath.row];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)recognizer {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
