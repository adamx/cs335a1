//
//  QuickViewController.h
//  CS335
//
//  Created by Adam Tay on 6/08/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *linksTableView;
@property (nonatomic, strong) NSArray *linksArray;
@property (nonatomic, strong) NSArray *urlArray;

@end
