//
//  CS335AppDelegate.h
//  CS335
//
//  Created by Adam Tay on 26/07/13.
//  Copyright (c) 2013 Adam Tay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CS335AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
